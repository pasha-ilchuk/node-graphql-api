const Validator = require('validatorjs');

module.exports = (input, rules, message = []) => new Promise((res) => {
  const validator = new Validator(input, rules, message);
  validator.passes(res);
  validator.fails(() => {
    res(validator.errors);
  });
});
