const JWTService = require('./JWTService');
const validate = require('./Validator');

module.exports = {
  JWTService,
  validate
};
