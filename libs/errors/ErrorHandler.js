module.exports = (err) => {
  const { originalError } = err;

  // handle GraphQL errors
  if (!originalError) {
    return {
      message: err.message
    };
  }

  let result = {};

  switch (originalError.name) {
  case 'CustomError':
    result = {
      message: originalError.message,
      type: originalError.name
    };
    break;
  default:
    result = {
      message: process.env.NODE_ENV === 'production' ? 'Something went wrong' : err.message
    };
    break;
  }
  return result;
};
