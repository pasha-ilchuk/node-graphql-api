const config = {
  app: {
    jwt: {
      expireDuration: 86400 * 60 // 2 months
    }
  }
};

module.exports = config;
