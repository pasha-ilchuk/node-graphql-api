const faker = require('faker');

// Bcrypted string for password: 123456
const password = '$2a$08$AG0JJ5WquAZFbmuTClhSoeDc8NIXs30y5JS17.T.F.zD7HNdAuyNa';
const refDate = '2018-01-01';

const users = [
  {
    id: 1,
    first_name: 'Admin',
    last_name: 'Admin',
    email: 'admin@mail.com',
    password,
    dob: faker.date.past(25, refDate),
    created_at: new Date()
  },
  {
    id: 2,
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: 'user2@mail.com',
    password,
    dob: faker.date.past(25, refDate),
    created_at: new Date()
  },
  {
    id: 3,
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: 'user3@mail.com',
    password,
    dob: faker.date.past(25, refDate),
    created_at: new Date()
  },
  {
    id: 4,
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: 'user4@mail.com',
    password,
    dob: faker.date.past(25, refDate),
    created_at: new Date()
  }
];

exports.seed = async (knex) => {
  await knex('users').insert(users);
};

exports.seedData = {
  users
};
