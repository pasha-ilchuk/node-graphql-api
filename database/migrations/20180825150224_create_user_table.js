exports.up = knex => knex.schema.createTable('users', (t) => {
  t.increments();
  t.string('first_name', 30).notNullable();
  t.string('last_name', 30).notNullable();
  t.string('email', 50)
    .notNullable()
    .unique();
  t.string('password', 60).notNullable();
  t.date('dob').notNullable();
  t.string('confirm_token', 32)
    .nullable()
    .comment('Encoded to md5');
  t.timestamp('password_requested_at').nullable();
  t.timestamp('created_at')
    .nullable()
    .defaultTo(null);
  t.timestamp('updated_at')
    .nullable()
    .defaultTo(null);
});

exports.down = knex => knex.schema.dropTableIfExists('users');
