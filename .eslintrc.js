module.exports = {
  extends: [
    "airbnb-base",
    // "prettier"
  ],
  rules: {
    'max-len': ['error', { code: 120 }],
    'no-underscore-dangle': [0],
    'no-param-reassign': ["error", { "props": false }],
    'comma-dangle': ['error', 'never'],
    camelcase: ["off"],
    quotes: ['error', 'single'],
    indent: ['error', 2],
    // "prettier/prettier": [
    //   "error",
    //   { "singleQuote": true },
    //   {"eslintIntegration": true}
    // ]
  },
  // plugins: [
  //   "prettier"
  // ]
};