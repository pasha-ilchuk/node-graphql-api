const { userCtrl, authCtrl } = require('./controllers');

const routes = {
  /*
    ---- graphiql example of queries ----
    query user($userID: Int!) {
      user(id: $userID) {
        id
        firstName
      }
    }
    {
      "userID": 1
    }

    {
      user(id:1){
        id
        firstName
      }
    }
   */
  user: userCtrl.get,
  users: userCtrl.list,
  signin: authCtrl.signin,
  signup: authCtrl.signup
};

module.exports = routes;
