const { bookshelf } = require('./../config/database');
const { comparePassword, hashPassword } = require('./../libs/');

const User = bookshelf.Model.extend(
  {
    tableName: 'users',
    comparePassword(plainPassword) {
      return comparePassword(plainPassword, this.get('password'));
    },
    hashPassword: async (model) => {
      const hash = await hashPassword(model.attributes.password);
      await model.set('password', hash);
    }
  },
  {
    rules: {
      signin: {
        email: 'required|email',
        password: 'required|min:6'
      },
      signup: {
        firstName: 'required|min:2|max:30|alpha_num',
        lastName: 'required|min:2|max:30|alpha_num',
        email: 'required|email',
        password: 'required|min:6',
        confirmPassword: 'required|same:password',
        dob: 'required|date'
      }
    }
  },
);

module.exports = bookshelf.model('User', User);
