require('./validate_env');

const Koa = require('koa');
const Router = require('koa-router');
const graphqlHTTP = require('koa-graphql');
const schema = require('./schema');
const routes = require('./routes');
const logger = require('./libs/Logger')(module);
const { ErrorHandler } = require('./libs/errors');

const app = new Koa();
const router = new Router();

router.all(
  '/api/graphql',
  graphqlHTTP({
    schema,
    rootValue: routes,
    graphiql: process.env.NODE_ENV === 'development',
    formatError: ErrorHandler
  }),
);

app.use(router.routes()).use(router.allowedMethods());

if (!module.parent) {
  app.listen(4000, () => {
    logger.info(`App running on port: ${process.env.PORT}`);
  });
}

module.exports = app;
