const { buildSchema } = require('graphql');

// https://stackoverflow.com/questions/49693928/date-and-json-in-type-definition-for-graphql
// https://www.apollographql.com/docs/graphql-tools/scalars.html
// https://medium.com/the-graphqlhub/graphql-tour-interfaces-and-unions-7dd5be35de0d
const schema = buildSchema(`
    scalar Date
    type Query {
        user(id: Int!): User
        users: [User]
    }
    type Mutation {
        signin(email: String!, password: String!): SigninResponse
        signup(email: String!, password: String!, confirmPassword: String!, firstName: String!, 
            lastName: String!, dob: Date!): GeneralMutationResponse
    }
    type ValidationError {
        name: String!
        message: [String!]!
    }
    interface GeneralMutationResponseInterface {
        ok: Boolean!
        errors: [ValidationError!]
    }
    type GeneralMutationResponse implements GeneralMutationResponseInterface {
        ok: Boolean!
        errors: [ValidationError!]
    }
    type SigninResponse implements GeneralMutationResponseInterface {
        ok: Boolean!
        errors: [ValidationError!]
        token: String
    }
    type User {
      id: Int
      firstName: String
      lastName: String
      email: String
      password: String
      dob: Date
    }
`);

module.exports = schema;
