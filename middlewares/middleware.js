const { JWTService, validate } = require('./../libs');
const { User } = require('./../models');
const { CustomError } = require('./../libs/errors');

const createResolver = (resolver) => {
  const baseResolver = resolver;
  baseResolver.createResolver = (childResolver) => {
    const newResolver = async (args, context) => {
      await resolver(args, context);
      return childResolver(args, context);
    };
    return createResolver(newResolver);
  };
  return baseResolver;
};

const requiresAuth = createResolver(async (args, context) => {
  const result = await validate(
    context.header,
    {
      'x-api-token': 'required|regex:/^Bearer\\s{1}([A-z\\.0-9-]+)$/'
    }
  );
  if (typeof result !== 'undefined') {
    throw new CustomError('Invalid header');
  }
  const [, token] = context.header['x-api-token'].split(' ');
  const decoded = await JWTService.verifyToken(token);
  context.state.user = await User.where({ id: decoded.data.id });
  context.state.user_id = decoded.data.id;
});

const requiresAdmin = requiresAuth.createResolver((args, context) => {
  if (!context.state.user.isAdmin) {
    throw new CustomError('Requires admin access');
  }
});

module.exports = {
  requiresAuth,
  requiresAdmin
};
