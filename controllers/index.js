const userCtrl = require('./user');
const authCtrl = require('./auth');

module.exports = {
  userCtrl,
  authCtrl
};
