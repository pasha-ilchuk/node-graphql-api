const { isNull } = require('lodash');
const { User } = require('./../models');
const { validate, JWTService } = require('./../libs');
const { CustomError } = require('./../libs/errors');

const handler = {
  signin: async (args) => {
    const validationResult = await validate(args, User.rules.signin);
    if (typeof validationResult !== 'undefined') {
      return { ok: false, errors: validationResult };
    }

    const { email, password } = args;

    const user = await User.where({ email }).fetch();
    if (isNull(user)) {
      throw new CustomError('Email not found');
    }
    if (!(await user.comparePassword(password))) {
      throw new CustomError('Wrong password');
    }

    return { ok: true, errors: [], token: `Bearer ${JWTService.signUser(user)}` };
  },
  signup: async (args) => {
    const validationResult = await validate(args, User.rules.signup);
    if (typeof validationResult !== 'undefined') {
      return { ok: false, errors: validationResult };
    }

    const {
      email,
      password,
      firstName: first_name,
      lastName: last_name,
      dob
    } = args;

    const usersCount = await User.where({ email }).count();
    if (usersCount) {
      throw new CustomError('User with this email already registered');
    }

    await new User({
      first_name,
      last_name,
      email,
      password,
      dob
    }).save();

    return { ok: true, errors: [] };
  }
};

module.exports = handler;
