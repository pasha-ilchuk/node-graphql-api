const moment = require('moment');
const { User } = require('./../models');
const { requiresAuth } = require('./../middlewares/middleware');
const { CustomError } = require('./../libs/errors');

const handler = {
  get: requiresAuth.createResolver(async (args) => {
    const { id } = args;
    const user = await User.where({ id }).fetch();
    if (!user) {
      throw new CustomError(`Couldn't find user with id ${id}`);
    }
    return {
      id: user.get('id'),
      firstName: user.get('first_name'),
      lastName: user.get('last_name'),
      dob: moment(user.get('dob')).format('YYYY-MM-DD')
    };
  }),
  list: async () => {
    const users = await User.fetchAll();
    return users.map(user => ({
      id: user.get('id'),
      firstName: user.get('first_name'),
      lastName: user.get('last_name'),
      dob: moment(user.get('dob')).format('YYYY-MM-DD')
    }));
  }
};

module.exports = handler;
