GraphQL middlewares libraries
https://github.com/thebigredgeek/apollo-resolvers
https://www.youtube.com/watch?v=LnOemRSax4A&index=243&t=2s&list=WL
https://github.com/prisma/graphql-middleware
https://github.com/alekbarszczewski/graphql-add-middleware#readme

GraphQL auth
https://blog.cloudboost.io/graphql-users-authorization-with-json-web-tokens-on-express-graphql-86d3dbf413df

GraphQL http server with context
graphqlHTTP(context => ({
   schema,
   rootValue: routes,
   graphiql: process.env.NODE_ENV === 'development'
   context
}))